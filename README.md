# Front-end de uma cafeteria utilizando HTML5 e CSS3 

Neste projeto tentei desenvolver do zero uma página para testar as regras de CSS3 que aprendi olhando alguns tutoriais do _Youtuber Dev Ed_ (https://www.youtube.com/channel/UClb90NQQcskPUGDIXsQEz5Q).

**Conceitos desenvolvidos:**
* Utilização de fontes externas;
* Margin, padding, border;
* Box-sizing;
* Opacidade de imagens;
* Gradient text;
* Border radius;
* Sombra em imagens;
* Posicionamento de elementos com z-index;
* Display flex/block;
* Sistema de grid;
* Media query;

**Ferramentas utilizadas:**

* Visual Studio Code Versão 1.54.1;
* Navegador Google Chrome Versão 89.0.4389.82;
* Windows 10 Versão 1909 (OS Build 18363.1379);

**Para visualizar as funcionalidades do site, baixe todos os arquivos em uma mesma pasta e abra o arquivo _index.html_ no seu navegador. Se preferir, na pasta _Apresentação Site_ tem algumas screenshots do site.**



